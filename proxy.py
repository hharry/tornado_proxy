from traceback import format_tb
import tornado.ioloop
from tornado.httpclient import AsyncHTTPClient, HTTPRequest, HTTPClientError
from tornado.web import RequestHandler, Application, HTTPError



PROXY_HOST = 'localhost'
PROXY_PORT = '8000'
TIMEOUT = 30
DEBUG = True



class BaseHandler(RequestHandler):

    def initialize(self, debug, timeout):
        self.debug = debug
        self.timeout = timeout
        self.client = AsyncHTTPClient()

    def set_default_headers(self):
        # Remove our server banners
        self.clear_header('Server')

    # Custom error handler to provide standard JSON error response
    def write_error(self, status_code, **kwargs):
        _, err, traceback = kwargs['exc_info']

        # "Safe" exceptions, return message and args
        if isinstance(err, HTTPError):
            self.write({
                'code': err.status_code,
                'message': err.log_message,
                'data': err.args
            })
            self.set_status(err.status_code)

        else:
            # Unknown exceptions, log and hide potentially dangerous
            # error messages

            if self.debug:
                self.write({
                    'code': 500,
                    'message': str(err),
                    'data': ''.join(format_tb(traceback))
                })
                self.set_status(500)

            else:
                self.write({
                    'code': 500,
                    'message': "Unexpected error.",
                    'data': None
                })
                self.set_status(500)


class NotFoundHandler(RequestHandler):
    # Override this instead of get() to cover all possible HTTP methods.
    def write_error(self, status_code, **kwargs):
        self.write({
            'code': 404,
            'message': "Not Found",
            'data': None
        })
        self.set_status(404)

    def set_default_headers(self):
        # Remove our server banners
        self.clear_header('Server')


class MainHandler(BaseHandler):
    def get(self, *args, **kwargs):
        self.write({"message": "It's working"})


class ProxyHandler(BaseHandler):

    # Headers that won't be proxied
    # use lower cased headers
    BLACKLISTED_HEADERS = ['host']
    BLACKLISTED_RESPONSE_HEADERS = {
        'content-length',
        'connection',
        'transfer-encoding'
    }

    async def do_request(self, method, url, **kwargs):

        if not url:
            raise HTTPError(400, "No URL provided.")

        try:

            req = HTTPRequest(
                url,
                method.upper(),
                connect_timeout=self.timeout,
                request_timeout=self.timeout,
                follow_redirects=False,
                **kwargs
            )
            return await self.client.fetch(req)


        except HTTPClientError as e:

            # Proxy HTTP errors as well, unless it is a timeout exception
            response = e.response

            if e.code == 599:
                raise HTTPError(504, "Request Timeout")

            return response

    def get_proxy_headers(self, headers):
        '''
        Modifies headers in place to allow only those that will be forwarded from the original request
        '''
        for h in self.BLACKLISTED_HEADERS:
            headers.pop(h, None)

        return headers

    def set_response_data(self, response):
        '''
        Set response data shared across all requests
        '''

        # TODO: Implement streaming responses, since this will OOM if the
        # response is too large
        if response.body is not None:
            self.write(response.body)

        # Headers
        bl = self.BLACKLISTED_RESPONSE_HEADERS
        for h, v in response.headers.items():
            if h.lower() not in bl:
                self.set_header(h, v)

        # Status code
        self.set_status(response.code)

    async def get(self, url, *args, **kwargs):
        extra = {
            'headers': self.get_proxy_headers(self.request.headers)
        }

        res = await self.do_request('GET', url, **extra)

        self.set_response_data(res)


    async def post(self, url, *args, **kwargs):
        extra = {
            'headers': self.get_proxy_headers(self.request.headers),
            'body': self.request.body
        }

        res = await self.do_request('POST', url, **extra)

        self.set_response_data(res)


def make_app(debug, timeout):
    '''
    Creates the tornado app

    debug: if True, all error messages will be returned
    timeout: request timeout
    '''

    extras = {
        'debug': debug,
        'timeout': timeout,
    }

    return Application([
        (r"/", MainHandler, extras),
        (r"/%20proxy/(.*)", ProxyHandler, extras), # Not sure about this silly white-space
    ], debug=debug, default_handler_class=NotFoundHandler)


if __name__ == '__main__':

    # Tornado server
    app = make_app(DEBUG, TIMEOUT)
    app.listen(PROXY_PORT, PROXY_HOST)
    print("Listening %s:%s" % (PROXY_HOST, PROXY_PORT))
    tornado.ioloop.IOLoop.current().start()
