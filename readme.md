# Test code for ParseHub
    Simple HTTP proxy for GET/POST methods.
    Implemented with Tornado async framework to provide a highly efficient proxy implementation.

    Pending: Handle other methods, and implement request/response streaming.


## Requirements
    Python 3.6
    pip
    virtualenv


## Installation
    Note: Ensure virtualenv points to python3 installation

    virtualenv ./env
        or virtualenv3

    source ./env/bin/activate
        or on Windows: env\Scripts\activate.bat

    pip install -r requirements.txt

    dev:
        pip install pylint
        Optional: Setup visual studio code interpreter


## Running the web server locally
    Run: python proxy.py
        to start the tornado proxy server


## Test
    curl  http://localhost:8000/ proxy/http://httpbin.org/get
    curl -X POST -d asdf=blah http://localhost:8000/ proxy/http://httpbin.org/post